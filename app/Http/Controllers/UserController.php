<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
* 
*/
class UserController extends Controller
{
	
	function __construct()
	{
		# code...
	}

	public function index()
	{
		return User::all();
	}

	public function show($id)
	{
		$user = User::find($id);

		if($user) {
			return $user;
		}
	}

	public function store(Request $request)
	{
		$data = $request->all();

		$validator = Validator::make($data, [
			'lastname'	=> 'required|string',
			'firstname'	=> 'required|string',
			'email'		=> 'required|email|unique:users,email',
			'nickname'	=> 'required|string|unique:users,nickname',
			'password'	=> 'required|string',
		]);

		if (!$validator->fails()) {
			return User::create([
				'lastname' 	=> $data['lastname'],
				'firstname' => $data['firstname'],
				'email' 	=> $data['email'],
				'nickname'	=> $data['nickname'],
				'password'	=> Hash::make($data['password'])
			]);	
		}
	}

	public function update(Request $request, $id)
	{
		$user 	= User::find($id);

		if ($user) {
			$update = $request->all();
			$update['password'] = Hash::make($update['password']);

			$user->fill($update);
			$user->save();	
		}
	}

	public function destroy($id)
	{
		$user = User::find($id);

		if ($user) {
			$user->delete();
		}
	}

	public function restore($id)
	{
		$user = User::withTrashed()->find($id);

		if ($user) {
			$user->restore();
		}
	}
}
