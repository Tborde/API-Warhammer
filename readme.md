# Projet API Warhammer

## Installation :

*   `Composer install`

## Générer une clef :

*   `php artisan key:generate`

## Installation de la bases de données :

*   Créer un base de donnée
*   Décommenter le .env.exemple et renseigner les informations
*   `php artisan migrate`
